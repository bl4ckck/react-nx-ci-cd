import { API_V1 } from '@react-nx/bookstore/environments';
import fetch from 'cross-fetch';
import { ResponseType } from '../schema';
import { BookType } from './book.schema';

export async function getBooks(): Promise<ResponseType<BookType[]>> {
  const data = await fetch(`${API_V1}/book`, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return data.json();
}

import { API_V1 } from '@react-nx/bookstore/environments';
import fetch from 'cross-fetch';
import { BookType } from '../book';
import { ResponseType } from '../schema';
import { OrderType } from './order.schema';

export async function createCheckout(
  payload: BookType[]
): Promise<ResponseType<OrderType>> {
  const data = await fetch(`${API_V1}/order`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(payload),
  });

  return data.json();
}

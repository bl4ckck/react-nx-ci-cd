export const API_URL = process.env['NX_API_URL'] || 'http://0.0.0.0:3000';
export const API_V1 = API_URL + '/api/v1';

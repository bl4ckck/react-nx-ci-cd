import { build } from './app';

const server = build();
const port = Number(process.env.PORT) || 3000;
const host = '0.0.0.0';

server.listen({ port, host }, function (err, address) {
  if (err) {
    server.log.error(err);
    process.exit(0);
  }
});
